This project is the source code for the Make File Tutorial blog at http://fernleaf07.blogspot.com/

There is a directory for each makefile in the blog.

Make1 directory holds makefile1 and supporting files.
Make2 directory holds makefile2 and supporting files.
etc.

Each directory is self contained.

Hope you find this useful.

Keith Smith
July 2015